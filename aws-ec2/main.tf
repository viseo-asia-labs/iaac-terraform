provider "aws" {
  region = "ap-southeast-1"
}

##################################################################
# Data sources to get VPC, subnet, security group and AMI details
##################################################################

variable "vpc_id" {
  default = "vpc-391dea5d"
}

data "aws_vpc" "default" {
  # default = true
  id = "${var.vpc_id}"
}

data "aws_subnet_ids" "all" {
  vpc_id = "${data.aws_vpc.default.id}"
}

data "aws_route53_zone" "selected" {
  name = "nellmedina.com."
}

data "aws_ami" "ubuntu" {
  most_recent = true

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-xenial-16.04-amd64-server-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["099720109477"]
}

module "security_group" {
  source      = "../aws-security-group"

  name        = "example"
  description = "Security group for example usage with EC2 instance"
  vpc_id      = "${data.aws_vpc.default.id}"

  ingress_cidr_blocks = ["0.0.0.0/0"]
  ingress_rules       = ["http-80-tcp", "all-icmp", "ssh-tcp"]
  egress_rules        = ["all-all"]
}

# for convenience, you can use your local public key to connect
resource "aws_key_pair" "ssh-1" {
  key_name = "ssh-1"
  public_key = "${file("/Users/marinellmedina/.ssh/id_rsa.pub")}"
}

# note that the eip resource will not update the proper DNS name in Route53 CNAME records
//resource "aws_eip" "this" {
//  vpc      = true
//  instance = "${module.ec2.id[0]}"
//}

resource "aws_route53_record" "the_a_record" {
  zone_id = "${data.aws_route53_zone.selected.zone_id}"
  name    = "${data.aws_route53_zone.selected.name}"
  type    = "A"
  ttl     = "300"
  records = ["${module.ec2.public_ip[0]}"]
}

resource "aws_route53_record" "www" {
  zone_id = "${data.aws_route53_zone.selected.zone_id}"
  name    = "www.${data.aws_route53_zone.selected.name}"
  type    = "CNAME"
  ttl     = "300"
  records = ["${module.ec2.public_dns[0]}"]
}

resource "aws_route53_record" "mario" {
  zone_id = "${data.aws_route53_zone.selected.zone_id}"
  name    = "mario.${data.aws_route53_zone.selected.name}"
  type    = "CNAME"
  ttl     = "300"
  records = ["${module.ec2.public_dns[0]}"]
}

//resource "aws_route53_record" "iana" {
//  zone_id = "${data.aws_route53_zone.selected.zone_id}"
//  name    = "iana.${data.aws_route53_zone.selected.name}"
//  type    = "CNAME"
//  ttl     = "300"
//  records = ["${module.ec2.public_dns[1]}"]
//}

//resource "aws_route53_record" "nina" {
//  zone_id = "${data.aws_route53_zone.selected.zone_id}"
//  name    = "nina.${data.aws_route53_zone.selected.name}"
//  type    = "CNAME"
//  ttl     = "300"
//  records = ["${module.ec2_with_t2_unlimited.public_dns[0]}"]
//}

# example of iterating from a List
//variable "example_gsuite" {
//  type = "list"
//  default = ["mail", "cal"]
//}
//resource "aws_route53_record" "example_gsuite" {
//  count   = "${length(var.example_gsuite)}"
//  zone_id = "${data.aws_route53_zone.selected.zone_id}"
//  name    = "${element(var.example_gsuite, count.index)}"
//  type    = "CNAME"
//  records = ["${aws_route53_record.nina.name}"]
//  ttl     = "300"
//}

module "ec2" {
  source = "./module"

  instance_count = 1

  name                        = "example-normal"
  ami                         = "${data.aws_ami.ubuntu.id}"
  instance_type               = "t2.micro"
  subnet_id                   = "${element(data.aws_subnet_ids.all.ids, 0)}"
  vpc_security_group_ids      = ["${module.security_group.this_security_group_id}"]
  associate_public_ip_address = true

  key_name = "${aws_key_pair.ssh-1.key_name}"
}

module "ec2_with_t2_unlimited" {
  source = "./module"

  instance_count = 1

  name                        = "example-t2-unlimited"
  ami                         = "${data.aws_ami.ubuntu.id}"
  instance_type               = "t2.micro"
  cpu_credits                 = "unlimited"
  subnet_id                   = "${element(data.aws_subnet_ids.all.ids, 0)}"
  vpc_security_group_ids      = ["${module.security_group.this_security_group_id}"]
  associate_public_ip_address = true
}