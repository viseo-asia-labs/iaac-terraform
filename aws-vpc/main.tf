provider "aws" {
  region = "ap-southeast-1"
}

module "vpc" {
  source = "./module"

  name = "no-private-subnets"

  cidr = "10.0.0.0/16"

  azs                 = ["ap-southeast-1a", "ap-southeast-1b", "ap-southeast-1c"]
  public_subnets      = ["10.0.0.0/22", "10.0.4.0/22", "10.0.8.0/22"]
  private_subnets     = []
  # database_subnets    = ["10.0.128.0/24", "10.0.129.0/24"]

  enable_dns_support   = true
  enable_dns_hostnames = true
  enable_nat_gateway   = false

  tags = {
    Name  = "simple-vpc"
  }
}


# to check for AZ in a Region:
# aws ec2 describe-availability-zones --region ap-southeast-1