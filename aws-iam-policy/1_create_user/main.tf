provider "aws" {
  region = "ap-southeast-1"
}

module "iam_user" {
  source = "./module"

  name          = "terraform-user-1"
  force_destroy = true

  path = "/nellmedina/"

  # User "nellmedina" has uploaded his public key here -> `curl https://keybase.io/nellmedina/pgp_keys.asc | gpg --import`
  pgp_key = "keybase:nellmedina"

  password_reset_required = false

  # SSH public key
  upload_iam_user_ssh_key = true

  ssh_public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQC190i7TLDrUVzcHKpuAGdl/ldYPpVOl4iB/ArMLq6KkuiHraQtXcZs/RbIOO5pSD/fNgllbCsBgK1aDtYq0uyos1EGlmDiYQ68nZgOXlw6gKGj79A5C2mHw8yUPxbAW+nY9sLJMI7iHDgi7tIdsurzBgtyBBALKC4hYzxod82cxu6eZaB7zOVNHxMMp5Z+THMsaNZl317Ibc8drEaynC9WAbju0pmAUC9k2Plg+09l03h7iF7VXE0zUT0Z2Pry3LHYCIqaKV3f68QeBZhXg0KFDxTZb0xu0EzWoB31gHY7qDdEvMZA92r46h86PmQn4Skj7i/x7MWyI3ZXR/a+xve1WvnmeZzak0E7aQ/nDcIrTDF3kx7UKK93WJfl3dCbr1jRKDRurr0Ozl0V8PukUwl3r5ZFAQtUYwtVQzfdJf4isXSusMreh1cgJdfBVoTUki+zWvneoyrVRr/reQUeNJRI1eImpPoOHcFvhoNhCMXjG8QpJjQ6DbGc2ZrS9QVUJcW8pZtNfR5CBVS4xKTodx33wlaFD1wbYTzqf/PAQb4rSJlXEKi9m+0ZPtJwtiD9c/4atGNb5msGuwM9Uusc7+f1pGmBEhgCqWhA7C8yb+rBjHHSWxtvTcLc9Zt4LkgMq/so1fBq5TC6CL6yqI+9V8UOyP4wZbHV1oNBU9CyKkbyGw=="

}
