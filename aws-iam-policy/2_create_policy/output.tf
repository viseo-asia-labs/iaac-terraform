output "this_aws_iam_policy_ec2" {
  value       = "${module.create_policies.this_aws_iam_policy_ec2}"
}

output "this_aws_iam_policy_iam" {
  description = "The IAM policy"
  value       = "${module.create_policies.this_aws_iam_policy_iam}"
}

output "this_aws_iam_policy_s3" {
  value       = "${module.create_policies.this_aws_iam_policy_s3}"
}

output "this_aws_iam_policy_cloudformation" {
  value       = "${module.create_policies.this_aws_iam_policy_cloudformation}"
}

output "this_aws_iam_policy_route53" {
  value       = "${module.create_policies.this_aws_iam_policy_route53}"
}
