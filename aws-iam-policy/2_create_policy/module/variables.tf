variable "terraform_username" {
  default = ""
}

variable "create_terraform_user" {
  default = false
}

variable "path" {
  description = "Desired path for the IAM user"
  default     = "/"
}
