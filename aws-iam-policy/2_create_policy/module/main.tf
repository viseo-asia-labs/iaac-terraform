data "aws_iam_policy_document" "terraform_policy_document_s3" {
  statement {
    sid = "terraformPolicy1"

    effect = "Allow"

    actions = [
      "s3:*"
    ]

    resources = [
      "*"
    ]
  }
}

data "aws_iam_policy_document" "terraform_policy_document_ec2" {
  statement {
    sid = "terraformPolicy2"

    effect = "Allow"

    actions = [
      "ec2:*"
    ]

    resources = [
      "*"
    ]
  }
}

data "aws_iam_policy_document" "terraform_policy_document_iam" {
  statement {
    sid = "terraformPolicy3"

    effect = "Allow"

    actions = [
      "iam:*"
    ]

    resources = [
      "*"
    ]
  }
}

data "aws_iam_policy_document" "terraform_policy_document_cloudformation" {
  statement {
    sid = "terraformPolicy4"

    effect = "Allow"

    actions = [
      "cloudformation:*"
    ]

    resources = [
      "*"
    ]
  }
}

data "aws_iam_policy_document" "terraform_policy_document_route53" {
  statement {
    sid = "terraformPolicy5"

    effect = "Allow"

    actions = [
      "route53:*"
    ]

    resources = [
      "*"
    ]
  }
}

data "aws_iam_policy_document" "terraform_policy_document_rds" {
  statement {
    sid = "terraformPolicy6"

    effect = "Allow"

    actions = [
      "rds:*"
    ]

    resources = [
      "*"
    ]
  }
}

resource "aws_iam_policy" "terraform_policy_ec2" {
  count = "${var.create_terraform_user ? 1 : 0}"

  name   = "terraform-policy-ec2"
  path   = "${var.path}"
  policy = "${data.aws_iam_policy_document.terraform_policy_document_ec2.json}"
}

resource "aws_iam_policy" "terraform_policy_s3" {
  count = "${var.create_terraform_user ? 1 : 0}"

  name   = "terraform-policy-s3"
  path   = "${var.path}"
  policy = "${data.aws_iam_policy_document.terraform_policy_document_s3.json}"
}

resource "aws_iam_policy" "terraform_policy_iam" {
  count = "${var.create_terraform_user ? 1 : 0}"

  name   = "terraform-policy-iam"
  path   = "${var.path}"
  policy = "${data.aws_iam_policy_document.terraform_policy_document_iam.json}"
}

resource "aws_iam_policy" "terraform_policy_cloudformation" {
  count = "${var.create_terraform_user ? 1 : 0}"

  name   = "terraform-policy-cloudformation"
  path   = "${var.path}"
  policy = "${data.aws_iam_policy_document.terraform_policy_document_cloudformation.json}"
}

resource "aws_iam_policy" "terraform_policy_route53" {
  count = "${var.create_terraform_user ? 1 : 0}"

  name   = "terraform-policy-route53"
  path   = "${var.path}"
  policy = "${data.aws_iam_policy_document.terraform_policy_document_route53.json}"
}

resource "aws_iam_policy" "terraform_policy_rds" {
  count = "${var.create_terraform_user ? 1 : 0}"

  name   = "terraform-policy-rds"
  path   = "${var.path}"
  policy = "${data.aws_iam_policy_document.terraform_policy_document_rds.json}"
}

resource "aws_iam_user_policy_attachment" "terraform_policy_attach_ec2" {
  count = "${var.create_terraform_user ? 1 : 0}"

  user    = "${var.terraform_username}"
  policy_arn = "${aws_iam_policy.terraform_policy_ec2.arn}"
}

resource "aws_iam_user_policy_attachment" "terraform_policy_attach_s3" {
  count = "${var.create_terraform_user ? 1 : 0}"

  user    = "${var.terraform_username}"
  policy_arn = "${aws_iam_policy.terraform_policy_s3.arn}"
}

resource "aws_iam_user_policy_attachment" "terraform_policy_attach_iam" {
  count = "${var.create_terraform_user ? 1 : 0}"

  user    = "${var.terraform_username}"
  policy_arn = "${aws_iam_policy.terraform_policy_iam.arn}"
}

resource "aws_iam_user_policy_attachment" "terraform_policy_attach_cloudformation" {
  count = "${var.create_terraform_user ? 1 : 0}"

  user    = "${var.terraform_username}"
  policy_arn = "${aws_iam_policy.terraform_policy_cloudformation.arn}"
}

resource "aws_iam_user_policy_attachment" "terraform_policy_attach_route53" {
  count = "${var.create_terraform_user ? 1 : 0}"

  user    = "${var.terraform_username}"
  policy_arn = "${aws_iam_policy.terraform_policy_route53.arn}"
}

resource "aws_iam_user_policy_attachment" "terraform_policy_attach_rds" {
  count = "${var.create_terraform_user ? 1 : 0}"

  user    = "${var.terraform_username}"
  policy_arn = "${aws_iam_policy.terraform_policy_rds.arn}"
}