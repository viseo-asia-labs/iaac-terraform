output "this_aws_iam_policy_ec2" {
  value       = "${element(concat(aws_iam_policy.terraform_policy_ec2.*.arn, list("")), 0)}"
}

output "this_aws_iam_policy_iam" {
  value       = "${element(concat(aws_iam_policy.terraform_policy_iam.*.arn, list("")), 0)}"
}

output "this_aws_iam_policy_s3" {
  value       = "${element(concat(aws_iam_policy.terraform_policy_s3.*.arn, list("")), 0)}"
}

output "this_aws_iam_policy_cloudformation" {
  value       = "${element(concat(aws_iam_policy.terraform_policy_cloudformation.*.arn, list("")), 0)}"
}

output "this_aws_iam_policy_route53" {
  value       = "${element(concat(aws_iam_policy.terraform_policy_route53.*.arn, list("")), 0)}"
}