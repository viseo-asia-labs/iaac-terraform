provider "aws" {
  region = "ap-southeast-1"
}

module "create_policies" {
  source = "./module"

  path= "/nellmedina/"

  create_terraform_user = true
  terraform_username = "terraform-user-1"

}
