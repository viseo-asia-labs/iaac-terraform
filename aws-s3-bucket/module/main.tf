provider "aws" {
  region = "ap-southeast-1"
}

data "aws_iam_policy_document" "s3_policy_document" {
  statement {
    sid = ""

    effect = "Deny"

    actions = [
      "s3:*"
    ]

    not_principals = {
      type = "AWS"
      identifiers = "${var.s3_iam_arns}"
    }

    resources = [
      "arn:aws:s3:::${var.s3_bucket_name}",
      "arn:aws:s3:::${var.s3_bucket_name}/*"
    ]
  }
}

resource "aws_s3_bucket" "s3_bucket" {
  bucket = "${var.s3_bucket_name}"
  acl    = "private"

  tags {
    Name        = "s3"
    Environment = "nellmedina"
  }

  policy = "${data.aws_iam_policy_document.s3_policy_document.json}"
}
