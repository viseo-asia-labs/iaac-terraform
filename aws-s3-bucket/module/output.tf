output "this_aws_s3_bucket" {
  description = "The s3 bucket"
  value = "${element(concat(aws_s3_bucket.s3_bucket.*.bucket, aws_s3_bucket.s3_bucket.*.bucket, list("")), 0)}"
}
