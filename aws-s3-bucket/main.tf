provider "aws" {
  region = "ap-southeast-1"
}

module "create_s3_bucket" {
  source = "./module"

  s3_bucket_name = "maven-repository-1"

  s3_iam_arns = [
    "arn:aws:iam::641278899178:root",
    "arn:aws:iam::641278899178:user/nellmedina/maven-user-1"
  ]

  path="/nellmedina/"
}
